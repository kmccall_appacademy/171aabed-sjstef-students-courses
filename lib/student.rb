require_relative './course.rb'

class Student
    
    def initialize(first_name, last_name)
        @first = first_name
        @last = last_name
        @courses = [] 
    end
    
    def first_name
        @first
    end
    
    def last_name
        @last
    end
    
    def name
        "#{@first} #{@last}"
    end
    
    def courses
        @courses
    end
    
    def enroll(course)
        conflicts = @courses.take_while{ |crse| course.conflicts_with?(crse)}
        if conflicts.empty?
            @courses << course
            @courses.last.students << self
        else
            raise "This course conflicts with your current schedule"
        end        
    end
    
    def course_load
        schedule = Hash.new{0}
        @courses.each{ |course| schedule[course.department] += course.credits }
        schedule
    end
    
end
